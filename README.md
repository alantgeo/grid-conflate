# grid-conflate

Conflates two datasets using a hex grid. Currently only identifies where geometries are missing from one dataset but were found in another dataset in the hex grid cell or its neighbours.

![Example of grid-conflate in action](example.png)

### Usage

    node --max-old-space-size=4000 index.js --output results a.geom b.geom

Where `a` and `b` are PostgreSQL tables with `geom` geometry fields. `results` will contain data from `a` which was missing in `b`.
