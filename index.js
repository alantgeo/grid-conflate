#!/usr/bin/env node

const { Pool } = require('pg')
const pool = new Pool()

const yargs = require('yargs')
const ndjson = require('ndjson')
const fs = require('fs')
const fc = require('@turf/helpers').featureCollection
const feature = require('@turf/helpers').feature
const asyncPool = require('tiny-async-pool')
const cloneDeep = require('lodash.clonedeep')
const simplify = require('simplify-way')

const argv = yargs(process.argv.slice(2))
  .option('n', {
    alias: 'limit',
    type: 'number',
    description: 'Process first N tiles only'
  })
  .option('c', {
    alias: 'single-cell',
    type: 'string',
    description: 'Process a single cell'
  })
  .option('d', {
    alias: 'debug',
    type: 'boolean',
    description: 'Turn on debug logging'
  })
  .option('o', {
    alias: 'output',
    type: 'string',
    description: 'Turn on debug logging',
    demandOption: true
  })
  .option('id', {
    type: 'string',
    description: 'Field used as MapRoulette Task ID'
  })
  .argv

if (argv._.length < 2) {
  console.error("Usage: ./index.js a b")
  process.exit(1)
}

const outputPath = argv.output

fs.mkdirSync(outputPath, { recursive: true })

const maxConcurrent = 8

const gridSize = 50 // meters

const arg1 = argv._[0].split('.')
const arg2 = argv._[1].split('.')

const aTable = arg1[0]
const bTable = arg2[0]

const aGeom = arg1.length > 1 ? arg1[1] : 'geom'
const bGeom = arg2.length > 1 ? arg2[1] : 'geom'

const outputKeys = [
  'grid',
  'missingInTarget',
  'mr_missingInTarget'
]

const outputStreams = {}
const outputStreamOutputs = {}

outputKeys.forEach(key => {
  outputStreams[key] = ndjson.stringify()
  outputStreamOutputs[key] = outputStreams[key].pipe(fs.createWriteStream(`${outputPath}/${key}.geojson`))
})

const missingInTarget = {}

function addWay(feature) {
  const ways = []
  if (feature.geometry.type === 'MultiLineString') {
    feature.geometry.coordinates.map(lineString => {
      const way = lineString.map(coordinate => coordinate.slice(0, 2).reverse().join(',')).join(';')
      ways.push(way)
    })
  } else if (feature.geometry.type === 'LineString') {
    const way = lineString.map(coordinate => coordinate.slice(0, 2).reverse().join(',')).join(';')
    ways.push(way)
  }
  const tags = []
  for (const [key, value] of Object.entries(feature.properties)) {
    tags.push(`${key}=${value}`)
  }
  return ways.map(way => `​http://localhost:8111/add_way?way=${way}${tags.length ? '&addtags=' : ''}${tags.join('%7C')}`)
}

async function main() {
  // select cells from the hexgrid which intersect with source A
  console.log(`Getting extent of ${aTable}`)
  console.time('time')
  const extentRes = await pool.query(`SELECT ST_Extent(${aGeom}) AS extent FROM ${aTable};`)
  console.timeEnd('time')
  const extent = extentRes.rows[0].extent.replace('BOX(', '').replace(')', '').replace(',', ' ').split(' ')

  process.stdout.write(`Building hexgrid cells covering ${aTable}...\n`)
  console.time('time')
  const cells = []

  const hexChunkSize = 50000 // meters
  const hexCellChunks = []
  const hexCellChunksCount = Math.ceil((extent[2] - extent[0]) / hexChunkSize) * Math.ceil((extent[3] - extent[1]) / hexChunkSize)
  let hexCellChunk = 1
  for (let x = Number(extent[0]); x < Number(extent[2]); x += hexChunkSize) {
    for (let y = Number(extent[1]); y < Number(extent[3]); y += hexChunkSize) {
      hexCellChunks.push([x, y])
    }
  }
  const generateHexCells = xy => new Promise(async resolve => {
    process.stdout.write(`${hexCellChunk} of ${hexCellChunksCount} (${Math.round(hexCellChunk / hexCellChunksCount * 100)}%)\r`)
    const rows = argv.c
      ? [{ id: argv.c }]
      : (await pool.query(`SELECT DISTINCT hex.i || '/' || hex.j AS id FROM ST_HexagonGrid(${gridSize}, ST_MakeEnvelope(${xy[0]}, ${xy[1]}, ${xy[0] + hexChunkSize}, ${xy[1] + hexChunkSize}, 3857)) AS hex INNER JOIN ${aTable} ON ST_Intersects(${aTable}.${aGeom}, hex.geom);`)).rows

    for (const row of rows) {
      cells.push(row)
    }
    hexCellChunk++

    resolve()
  })
  await asyncPool(maxConcurrent, hexCellChunks, generateHexCells)
  console.log('')
  console.timeEnd('time')

  const processTile = tile => new Promise(async resolve => {
    const i = tile.i
    const [x, y] = tile.id.split('/').map(i => Number(i))

    if (!argv.debug && process.stdout.isTTY && i % 1000 === 0) {
      process.stdout.write(`${i.toLocaleString()} of ${totalCells.toLocaleString()} (${Math.floor(i / totalCells * 100)}%)\r`)
    }

    if (argv.debug) {
      console.log(`Processing cell ${i} ${x}/${y}`)
      const cell = JSON.parse(
        (
          await pool.query(`
            SELECT
              ST_AsGeoJSON(
                ST_Transform(
                  ST_SetSRID(
                    ST_Hexagon(${gridSize}, ${x}, ${y})
                    , 3857
                  )
                  , 4326
                )
              ) AS geojson
          `)
        ).rows[0].geojson)

      // order used is center cell, N, S, NE, SE, NW, SW
      const cellNeighbours = JSON.parse(
        (
          await pool.query(`
            SELECT
              ST_AsGeoJSON(
                ST_Transform(
                  ST_Union(ARRAY[
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y}                      ), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y + 1}                  ), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y - 1}                  ), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y - 1 : y}), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
                    ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y - 1 : y}), 3857)
                  ])
                  , 4326
                )
              ) AS geojson
          `)
        ).rows[0].geojson)

      fs.writeFileSync('debug/cell.geojson', JSON.stringify(fc([feature(cell)])))
      fs.writeFileSync('debug/cellNeighbours.geojson', JSON.stringify(fc([feature(cellNeighbours)])))
    }
    const cellHexQuery = pool.query(`
      SELECT
        ST_AsGeoJSON(
          ST_Transform(
            ST_SetSRID(
              ST_Hexagon(${gridSize}, ${x}, ${y})
              , 3857
            )
            , 4326
          )
        ) AS geojson`)

    // find features from A within this cell
    const aQuery = pool.query(`
      SELECT
        *,
        ST_AsGeoJSON(ST_Transform(${aGeom}, 4326)) AS geojson
      FROM
        ${aTable}
      WHERE
        ST_Intersects(
          ${aTable}.${aGeom},
          ST_SetSRID(ST_Hexagon(${gridSize}, ${x}, ${y}), 3857)
        )
      ;`)

    // find features from B within this cell
    const bQuery = pool.query(`
      SELECT
        *,
        ST_AsGeoJSON(ST_Transform(${bGeom}, 4326)) AS geojson
      FROM
        ${bTable}
      WHERE
        ST_Intersects(
          ${bTable}.${bGeom},
          ST_SetSRID(ST_Hexagon(${gridSize}, ${x}, ${y}), 3857)
        )
      ;`)

    // find features from A within this cell or its neighbours
    // order used is center cell, N, S, NE, SE, NW, SW
    const aSearchQuery = pool.query(`
      SELECT
        *,
        ST_AsGeoJSON(ST_Transform(${aGeom}, 4326)) AS geojson
      FROM
        ${aTable}
      WHERE
        ST_Intersects(
          ${aTable}.${aGeom},
          ST_Union(ARRAY[
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y}                      ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y + 1}                  ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y - 1}                  ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y - 1 : y}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y - 1 : y}), 3857)
          ])
        )
      ;`)

    // find features from B within this cell or its neighbours
    const bSearchQuery = pool.query(`
      SELECT
        *,
        ST_AsGeoJSON(ST_Transform(${bGeom}, 4326)) AS geojson
      FROM
        ${bTable}
      WHERE
        ST_Intersects(
          ${bTable}.${bGeom},
          ST_Union(ARRAY[
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y}                      ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y + 1}                  ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x}    , ${y - 1}                  ), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x + 1}, ${x % 2 === 0 ? y - 1 : y}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y : y + 1}), 3857),
            ST_SetSRID(ST_Hexagon(${gridSize}, ${x - 1}, ${x % 2 === 0 ? y - 1 : y}), 3857)
          ])
        )
      ;`)

    const [cellHexRes, aRes, bRes, aSearchRes, bSearchRes] = [await cellHexQuery, await aQuery, await bQuery, await aSearchQuery, await bSearchQuery]
    const cellHex = JSON.parse(cellHexRes.rows[0].geojson)
    const aFeatures = aRes.rows
    const bFeatures = bRes.rows
    const aSearchFeatures = aSearchRes.rows
    const bSearchFeatures = bSearchRes.rows

    if (argv.debug) {
      fs.writeFileSync('debug/aFeatures.geojson', JSON.stringify(fc(aFeatures.map(p => feature(JSON.parse(p.geojson))))))
      fs.writeFileSync('debug/bFeatures.geojson', JSON.stringify(fc(bFeatures.map(p => feature(JSON.parse(p.geojson))))))
      fs.writeFileSync('debug/aSearchFeatures.geojson', JSON.stringify(fc(aSearchFeatures.map(p => feature(JSON.parse(p.geojson))))))
      fs.writeFileSync('debug/bSearchFeatures.geojson', JSON.stringify(fc(bSearchFeatures.map(p => feature(JSON.parse(p.geojson))))))
    }

    if (aFeatures.length || bFeatures.length) {
      const tileFeature = feature(
        cellHex,
        {
          result:
            aFeatures.length && !bSearchFeatures.length ? 'Missing in Target' : (
              aFeatures.length && bSearchFeatures.length ? 'Found in Both' : (
                bFeatures.length && !aSearchFeatures.length ? 'Only in Target' : null
              )
            )
        },
        {
          id: `${x}/${y}`
        }
      )
      outputStreams.grid.write(tileFeature)

      // missing in target
      if (aFeatures.length && !bSearchFeatures.length) {
        for (const featureProperties of aFeatures) {
          if (!missingInTarget[featureProperties.ogc_fid]) {
            // remove null values
            for (const [k, v] of Object.entries(featureProperties)) {
              if (v === null || v === undefined) {
                delete featureProperties[k]
              }
            }

            delete featureProperties[aGeom]
            const geometry = featureProperties.geojson
            delete featureProperties.geojson

            const feature = {
              type: 'Feature',
              properties: Object.assign({}, featureProperties),
              geometry: JSON.parse(geometry)
            }
            feature.id = featureProperties[argv.id]
            delete feature.properties[argv.id]
            delete feature.properties.ogc_fid

            missingInTarget[featureProperties.ogc_fid] = {
              feature,
              cells: []
            }
          }
          missingInTarget[featureProperties.ogc_fid].cells.push(cellHex)
        }
      }
    }
    resolve()
  })

  const chunkSize = 1000

  const uniqueCells = {}
  for (let i = 0; i < cells.length; i++) {
    uniqueCells[cells[i].id] = cells[i]
  }

  const totalCells = Object.values(uniqueCells).length
  console.log(`${totalCells.toLocaleString()} cells to process`)

  console.time('time')
  for (let chunkIndex = 0; chunkIndex < (totalCells / chunkSize); chunkIndex++) {
    const tilesToProcess = Object.values(uniqueCells).slice(chunkIndex * chunkSize, (chunkIndex + 1) * chunkSize)
      .map((tile, index) => {
        tile.i = (chunkIndex * chunkSize) + index
        return tile
      })

    await asyncPool(maxConcurrent, tilesToProcess, processTile)

    if (argv.n && (chunkIndex * chunkSize) > argv.n) {
      break
    }
  }
  await pool.end()
  console.timeEnd('time')

  console.log('Building results')
  console.time('time')
  for (const missing of Object.values(missingInTarget)) {
    const cellCount = missing.cells.length

    if (argv.id in missing.feature.properties) {
      missing.feature.id = missing.feature.properties[argv.id]
      delete missing.feature.properties[argv.id]
    }

    const wayLength = missing.feature.properties.way_length
    delete missing.feature.properties.way_length

    // JOSM remote control link
    // https://josm.openstreetmap.de/wiki/Help/RemoteControlCommands#add_way
    /*
    addWay(missing.feature).forEach((way, index) => {
      missing.feature.properties[`addWay_${index}`] = way
    })
    */
    const tolerance = 0.5
    const referenceFeature = cloneDeep(missing.feature)

    if (referenceFeature.geometry.type === 'LineString') {
      referenceFeature.geometry.coordinates = simplify(referenceFeature.geometry.coordinates, tolerance)
    } else if (referenceFeature.geometry.type === 'MultiLineString') {
      for (let i = 0; i < referenceFeature.geometry.coordinates.length; i++) {
        referenceFeature.geometry.coordinates[i] = simplify(referenceFeature.geometry.coordinates[i], tolerance)
      }
    }


    const missingPercentage = Number((cellCount * gridSize * 1.6 / wayLength * 100).toFixed(2))
    missing.feature.properties.missingPercentage = missingPercentage

    missing.feature.properties['stroke'] = '#e100ff'
    missing.feature.properties['stroke-width'] = 4
    missing.feature.properties['stroke-opacity'] = 1


    if (argv.debug) {
      console.log(`Missing in ${cellCount} cells with length ${Math.round(wayLength)}, ${missingPercentage}%`)
    }

    const task = {
      type: 'FeatureCollection',
      features: [
        ...missing.cells.map(cell => feature(cell, {
          'fill': '#f00',
          'fill-opacity': 0.25,
          'stroke': '#f00',
          'stroke-width': 2,
          'stroke-opacity': 1
        })),
        // last feature is used as the first one shown in MapRoulette
        missing.feature
      ],
      attachments: [
        {
          id: missing.feature.id,
          kind: 'referenceLayer',
          type: 'geojson',
          name: 'way',
          data: referenceFeature,
          settings: {
            layerLocked: false,
            uploadPolicy: true
          },
          purpose: 'If confirmed via imagery then it may be acceptable to use the source geometry in your edit, however care must be taken to ensure the way is simplified before uploading, connected to the existing road network for routing, and sections which already exist in OSM are not duplicated. Please review tags only those which you can be sure of should be included, if unsure best to omit it.'
        }
      ]
    }
    outputStreams.mr_missingInTarget.write(task)

    outputStreams.missingInTarget.write(missing.feature)
  }
  console.timeEnd('time')

  outputKeys.forEach(key => {
    outputStreams[key].end()
  })

  Promise.all(outputKeys.map(key => {
    return new Promise(resolve => {
      outputStreamOutputs[key].on('finish', () => {
        console.log(`saved ${outputPath}/${key}.geojson`)
        resolve()
      })
    })
  }))
    .then(() => {
      process.exit(0)
    })
}
main()
